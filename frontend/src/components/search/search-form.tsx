import React, {FC, useState} from 'react';


 interface IProps {
     onSubmit:(searchText:string ) => any
}


export const SearchForm: React.FC<IProps> = ({onSubmit}: IProps) => {

    const [text, setText] = useState('');
    return (
        <form onSubmit={(event)=>{
            event.preventDefault()
            onSubmit(text )
        }}>
            <input type={'search'} value={text}  onChange={(evt)=>setText(evt.target.value)} />
            <button type={'submit'}> Search</button>
        </form>

    );
};
