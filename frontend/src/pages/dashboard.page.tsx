import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import {SearchForm} from "../components/search/search-form";

const backendClient = new BackendClient();


export const DashboardPage: FC<RouteComponentProps> = () => {
  const [total, setTotal] = useState(0);
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState(true );


  const fetchData = async (text = '') => {
    const result = await backendClient.getAllUsers(text);
    setUsers(result.data);
    setTotal(result.total )
    setLoading(false)
  };

  useEffect(() => {
    fetchData();
  }, []);



  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>


            <SearchForm  onSubmit={(text)=>fetchData(text)}/>

            <p>
              {

              }

            </p>

            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}


          </div>
        )}
      </div>
    </div>
  );
};
