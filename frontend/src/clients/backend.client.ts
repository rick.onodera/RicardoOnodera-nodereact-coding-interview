import axios from "axios";
import { IUserProps } from "../dtos/user.dto";

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers( text = '', page = 1): Promise<{ data: IUserProps[] , total:number  }> {

    return (await axios.get(`${this.baseUrl}/people?text=${text}&page=${page}`, {})).data;
  }
}
