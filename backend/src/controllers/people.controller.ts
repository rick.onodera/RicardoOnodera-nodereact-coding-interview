import {
    JsonController,
    Get,
    HttpCode,
    NotFoundError,
    Param, QueryParam,
} from 'routing-controllers';
import { PeopleProcessing } from '../services/people_processing.service';

const peopleProcessing = new PeopleProcessing();

@JsonController('/people', { transformResponse: false })
export default class PeopleController {
    @HttpCode(200)
    @Get()
    getAllPeople(@QueryParam("text") text: string) {

        const result =  peopleProcessing.getAll(text);

        if (result.total === 0) {
            throw new NotFoundError('No people found');
        }

        return  result
    }

    @HttpCode(200)
    @Get('/:id')
    getPerson(@Param('id') id: number) {
        const person = peopleProcessing.getById(id);

        if (!person) {
            throw new NotFoundError('No person found');
        }

        return {
            data: person,
        };
    }
}
