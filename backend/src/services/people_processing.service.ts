import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll(text = '') {

        const total = people_data.length

        if (!text) {
            return { data: people_data , total }
        }

        const data =  people_data.filter(doc => doc.first_name.toLowerCase().includes(text))

        return {
            data ,
            total
        }
    }
}
